import React, { Component } from 'react';
import CountriesApp from './containers/CountriesApp/CountriesApp';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <CountriesApp/>
      </div>
    );
  }
}

export default App;
