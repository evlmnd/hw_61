import React, {Component, Fragment} from 'react';
import CountryItem from '../../components/CountryItem/CountryItem';
import FullCountryInfo from '../../components/FullCountryInfo/FullCountryInfo';
import axios from 'axios';


import './CountriesApp.css';

class CountriesApp extends Component {

    state = {
        countries: [],
        selectedCountry: {}
    };

    selectCountry = (country) => {
        axios.get("/alpha/" + country)
            .then(responseCountry => {
                const country = responseCountry.data;
                Promise.all(country.borders.map(border => axios.get("/alpha/" + border)))
                    .then(responseBorder => {
                        country.borders = responseBorder.map(border => border.data.name);
                        this.setState({selectedCountry: country})
                    })
            }).catch(error => {
            console.log(error);
        });
    };

    componentDidMount() {
        const COUNTRIES_URL = '/all?fields=name;alpha3Code';

        axios.get(COUNTRIES_URL).then(response => {
            this.setState({countries: response.data});

        }).catch(error => {
            console.log(error);
        });

    }

    render() {

        return (
            <Fragment>
                <div className="List">
                    {this.state.countries.map(country => {
                        return <CountryItem
                            countryName={country.name}
                            clicked={() => this.selectCountry(country.alpha3Code)}
                            key={country.name}
                            id={country.alpha3Code}
                        />
                    })}
                </div>
                <div className="Info">
                    {Object.keys(this.state.selectedCountry).length > 0 ?
                        <FullCountryInfo country={this.state.selectedCountry}/> :
                        <p>Select country from the list to see info! :)</p>
                    }
                </div>
            </Fragment>
        );
    }
}

export default CountriesApp;