import React from 'react';

import './CountryItem.css';

const CountryItem = props => {
    return (
        <div onClick={props.clicked} className="Country-Item">
            {props.countryName}
        </div>
    );
};

export default CountryItem;