import React from 'react';

import './FullCountryInfo.css';

const FullCountryInfo = props => {
    return (
        <div>
            <h1>{props.country.name}</h1>
            <img src={props.country.flag} alt="flag" className="Flag"/>
            <p>Capital: {props.country.capital}</p>
            <p>Population: {props.country.population}</p>
            {props.country.borders.length > 0 ?
                <ul>{props.country.borders.map(border => <li key={"border-" + border}>{border}</li>)}</ul> :
                <p>This country has no border neighbours  :(</p>
            }
        </div>
    );
};


export default FullCountryInfo;